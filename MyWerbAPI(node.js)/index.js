
const express = require('express');
const app = express();
const router = express.Router();
const port = 3000;

var path = require('path');
var http = require("http");
var url = require("url");
var cors = require("cors");




app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(express.static(path.join(__dirname, '../')));

app.options('*', cors()); 

// all routes prefixed with /api
app.use('/api', router);

//send empty results
router.get('/empty', (request, response) => {
  response.json(
    {
      "userId": null,
      "id": null,
      "title": null,
      "completed": null
    }
  );
    }); 
//send large result
router.get('/largeResult', (request, response) => {
let _largeResults = [];
for(let i = 0; i < 10000; i++)
_largeResults.push(
  {
    "userId": i,
    "id": i,
    "title": "i am a part of a large result" ,
    "completed": false
  }
)
  response.json(_largeResults);
}); 

// set the server to listen on port 3000
app.listen(port, () => console.log(`Listening on port ${port}`));