
Use GameTableDB 
Go
Create table Games
(
ID int primary key identity,
GameName nvarchar(100),
Player1 nvarchar(50),
Player2 nvarchar(50),
Winner nvarchar(50),
)

Go

Insert into Games values('Chess', 'Max', 'Ilan', 'Max')
Insert into Games values('Scotland Yard', 'Philip', 'Kendi', 'Kendi')
Insert into Games values('Monopoly', 'Patrick', 'Ilana', 'Patrick')
Insert into Games values('Barricade', 'Mandy', 'Uli', 'Mandy')
Insert into Games values('Backgammon', 'Steve', 'Harel', 'Harel')
Insert into Games values('Code777', 'Lars', 'Lissy', 'Lissy')
Insert into Games values('Dame', 'Mike', 'John', 'John')
Insert into Games values('Dots', 'Oliver', 'Isla', 'Oliver')
Insert into Games values('Egghead', 'Harry', 'Jack', 'Jack')
Insert into Games values('Escalero', 'Grace', 'Oscar', 'Oscar')
Insert into Games values('Empire', 'Freddie', 'Sophie', 'Freddie')
Insert into Games values('Icecool', 'Lily', 'Jacob', 'Jacob')
Insert into Games values('Whist', 'Noah', 'Carolin', 'Carolin')
Insert into Games values('Trio', 'Harry', 'Gerge', 'Harry')
Insert into Games values('twenty one', 'Emily', 'Alfie', 'Emily')