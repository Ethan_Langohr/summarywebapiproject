﻿using OnlineGameService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OnlineGamesAPI.Controllers
{
    public class OnlineGameController : ApiController
    {
        //GET api/onlinegame
        public HttpResponseMessage Get()
        {
            using (GameTableDBEntities entities = new GameTableDBEntities())
            {
                return Request.CreateResponse(HttpStatusCode.OK, entities.Games.ToList());
            }
        }

        // GET api/onlinegame/5
        public HttpResponseMessage Get(int id)
        {
            using (GameTableDBEntities entities = new GameTableDBEntities())
            {
                Game food = entities.Games.FirstOrDefault(f => f.ID == id);
                if (food != null)
                    return Request.CreateResponse(HttpStatusCode.OK, food);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, String.Format("Game with ID {0} was not found", id));
            }
        }

        // POST api/onlinegame
        public HttpResponseMessage Post([FromBody]Game game)
        {
            if (game != null)
            {
                using (GameTableDBEntities entities = new GameTableDBEntities())
                {
                    entities.Games.Add(game);
                    entities.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.Created, new Uri(Request.RequestUri + "/" + game.ID.ToString()));
                }
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.NoContent, String.Format("Content in body is null.")); 
            }
            
        }

        // PUT api/onlinegame/5
        public HttpResponseMessage Put(int id, [FromBody]Game game)
        {
            using (GameTableDBEntities entities = new GameTableDBEntities())
            {
                Game gamestateToUpdate = entities.Games.FirstOrDefault(f => f.ID == id);
                if (gamestateToUpdate != null)
                {
                    if (game != null)
                    {
                        gamestateToUpdate.GameName = game.GameName;
                        gamestateToUpdate.Player1 = game.Player1;
                        gamestateToUpdate.Player2 = game.Player2;
                        gamestateToUpdate.Winner = game.Winner;

                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.Accepted, game);
                    }
                    else
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NoContent, String.Format("Content in body is null."));
                    }

                }
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, String.Format("Game with ID {0} was not found", id));
            }
        }

        // DELETE api/onlinegame/4
        public HttpResponseMessage Delete(int id)
        {
            using (GameTableDBEntities entities = new GameTableDBEntities())
            {
                Game game = entities.Games.FirstOrDefault(f => f.ID == id);
                if (game != null)
                {
                    entities.Games.Remove(game);
                    entities.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.Accepted, String.Format("Game with ID {0} was deleted", id));
                }
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, String.Format("Game with ID {0} was not found", id));
            }
        }

        //Get api/onlinegame/player
        [Route("api/onlinegame/player/{playerName}")]
        [HttpGet]
        public HttpResponseMessage GetByName(string playerName)
        {
            using (GameTableDBEntities entities = new GameTableDBEntities())
            {
                List<Game> playerList = entities.Games.Where(f => f.Player1.ToUpper().StartsWith(playerName.ToUpper()) || f.Player2.ToUpper().StartsWith(playerName.ToUpper())).ToList();
                if (playerList.Count > 0)
                    return Request.CreateResponse(HttpStatusCode.OK, playerList);
                else
                    return Request.CreateResponse(HttpStatusCode.NotFound, String.Format("Player with name {0} was not found", playerName));
            }
        }

        //Get api/onlinegame/search
        [Route("api/onlinegame/search")]
        [HttpGet]
        public HttpResponseMessage GetByFilter(int Id = -1, string gameName = null, string player1 = null, string player2 = null, string winner = null)
        {
            using (GameTableDBEntities entities = new GameTableDBEntities())
            {
                List<Game> gameList = entities.Games.Where(g => Id > -1 ? g.ID == Id : true)
                    .Where(g => gameName != null ? g.GameName.ToUpper().StartsWith(gameName.ToUpper()) : true)
                    .Where(g => player1 != null ? g.Player1.ToUpper().StartsWith(player1.ToUpper()) : true)
                    .Where(g => player2 != null ? g.Player2.ToUpper().StartsWith(player2.ToUpper()) : true)
                    .Where(g => winner != null ? g.Winner.ToUpper().StartsWith(winner.ToUpper()) : true)
                    .ToList();
                if (gameList.Count > 0)
                    return Request.CreateResponse(HttpStatusCode.OK, gameList);
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, String.Format("Searched game or player not found"));
                }
                    
            }
        }
    }
}
