﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineGamesAPI.Controllers
{
    public class PageController : Controller
    {

        public ActionResult GetPage()
        {
            return new FilePathResult("~/Views/Page/GetPage.html", "text/html");
        }

        public ActionResult GetPageTesting()
        {
            return new FilePathResult("~/Views/Page/GetPageTesting.html", "text/html");
        }

        public ActionResult GetJQuery()
        {
            return new FilePathResult("~/Views/Page/GetJQuery.html", "text/html");
        }

        public ActionResult GetObservables()
        {
            return new FilePathResult("~/Views/Page/GetObservables.html", "text/html");

        }

        public ActionResult PostPage()
        {
            return new FilePathResult("~/Views/Page/PostPage.html", "text/html");
        }

        public ActionResult PostJQuery()
        {
            return new FilePathResult("~/Views/Page/PostJQuery.html", "text/html");
        }

        public ActionResult PutPage()
        {
            return new FilePathResult("~/Views/Page/PutPage.html", "text/html");
        }

        public ActionResult PutJQuery()
        {
            return new FilePathResult("~/Views/Page/PutJQuery.html", "text/html");
        }

        // GET: Page
        public ActionResult Index()
        {
            return View();
        }
    }
}