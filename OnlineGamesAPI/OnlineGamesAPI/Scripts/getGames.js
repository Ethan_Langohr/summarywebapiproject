﻿function getGames() {
    var request = new XMLHttpRequest();
    var url = 'http://localhost:50966/api/onlinegame/'
    request.open('GET', url)
    request.onload = function () {
        var list = JSON.parse(request.response)

        document.getElementById("gameTable").innerHTML += "<tr><th>Id</th><th>Gamename</th><th>Player1</th><th>Player2</th><th>Winner</th></tr>"
        for (var i = 0; i < list.length; i++) {
            var string1 = "<tr>"
            var item = list[i]
            for (var field in item) {
                string1 += "<td>" + item[field] + "</td>"
            }
            string1 += "<tr/>"
            document.getElementById("gameTable").innerHTML += string1
        }
    }
    request.onerror = function () {
        alert("Failed to load items from gametable")
    }

    request.send()
}

function getGameById() {
    var request = new XMLHttpRequest();
    var url = 'http://localhost:50966/api/onlinegame/' + document.getElementById("ID").value.toString();
    
    request.open('GET', url)
    request.onload = function () {
        console.log(request.responseText)
        
        var list = JSON.parse(request.response)
        if (typeof list.ID != 'undefined') {
            document.getElementById("emptyId").innerHTML = ""
            document.getElementById("gameTable").innerHTML = ''
            document.getElementById("gameTable").innerHTML += "<tr><th>Id</th><th>Gamename</th><th>Player1</th><th>Player2</th><th>Winner</th></tr>"
            document.getElementById("gameTable").innerHTML += "<tr><td>" + list.ID + "</td><td>" + list.GameName + "</td><td>" + list.Player1 + "</td><td>" + list.Player2 + "</td><td>" + list.Winner + "</td></tr>"
        }
        else if (request.responseText.includes("not found")) {
            document.getElementById("emptyId").innerHTML = "Game with Id " + document.getElementById("ID").value.toString() + " not found"
        }
        else {
            document.getElementById("emptyId").innerHTML = "Please enter valid Id"
        }
    }
    request.onerror = function () {
        alert("Failed to load items from gametable")
    }
    
    request.send()
}

function getGamesJQuery() {
    $.ajax(
        {
            url: 'http://localhost:50966/api/onlinegame/'
        }).then(
        function (data) {
            console.log(data)
            var list = data;

            document.getElementById("gameTable").innerHTML += "<tr><th>Id</th><th>Gamename</th><th>Player1</th><th>Player2</th><th>Winner</th></tr>"
            for (var i = 0; i < list.length; i++) {
                var string1 = "<tr>"
                var item = list[i]
                for (var field in item) {
                    string1 += "<td>" + item[field] + "</td>"
                }
                string1 += "<tr/>"
                document.getElementById("gameTable").innerHTML += string1
            }
        }
        )
}

function getGamesJQueryById() {
    $.ajax(
        {
            url: 'http://localhost:50966/api/onlinegame/' + $('#ID').val()
        }).then(
            function (data) {
                var list = data

            if (typeof list.ID != 'undefined') {
                document.getElementById("emptyId").innerHTML = ""
                document.getElementById("gameTable").innerHTML = ''
                document.getElementById("gameTable").innerHTML += "<tr><th>Id</th><th>Gamename</th><th>Player1</th><th>Player2</th><th>Winner</th></tr>"
                document.getElementById("gameTable").innerHTML += "<tr><td>" + list.ID + "</td><td>" + list.GameName + "</td><td>" + list.Player1 + "</td><td>" + list.Player2 + "</td><td>" + list.Winner + "</td></tr>"
                document.getElementById("emptyId").innerHTML = ''
            }
           
            else {
                document.getElementById("emptyId").innerHTML = "Please enter valid Id"
            }
        }
        ).fail(
        function () { document.getElementById("emptyId").innerHTML = "Game with Id " + document.getElementById("ID").value.toString() + " not found"}
            )
}

function getGamesWithObservable() {

    url = 'http://localhost:50966/api/onlinegame/'

    subscription = Rx.Observable.from(fetch(url))
        .switchMap(response => Rx.Observable.from(response.json()))
        .subscribe(data => {
            var list = data;
            document.getElementById("gameTable").innerHTML += "<tr><th>Id</th><th>Gamename</th><th>Player1</th><th>Player2</th><th>Winner</th></tr>"
            for (var i = 0; i < list.length; i++) {
                var string1 = "<tr>"
                var item = list[i]
                for (var field in item) {
                    string1 += "<td>" + item[field] + "</td>"
                }
                string1 += "<tr/>"
                document.getElementById("gameTable").innerHTML += string1
            }
        });

    setTimeout(function () {
        subscription.unsubscribe();
    }, 5000);
}

function getGamesWithObservableById() {

    url = 'http://localhost:50966/api/onlinegame/' + $('#ID').val()

    subscription = Rx.Observable.from(fetch(url))
        .switchMap(response => Rx.Observable.from(response.json()))
        .subscribe(data => {
            var list = data;
            if (typeof list.ID != 'undefined') {
                document.getElementById("emptyId").innerHTML = ""
                document.getElementById("gameTable").innerHTML = ''
                document.getElementById("gameTable").innerHTML += "<tr><th>Id</th><th>Gamename</th><th>Player1</th><th>Player2</th><th>Winner</th></tr>"
                document.getElementById("gameTable").innerHTML += "<tr><td>" + list.ID + "</td><td>" + list.GameName + "</td><td>" + list.Player1 + "</td><td>" + list.Player2 + "</td><td>" + list.Winner + "</td></tr>"
                document.getElementById("emptyId").innerHTML = ''
            }

            else {
                document.getElementById("emptyId").innerHTML = "Please enter valid Id"
            }
        });

    setTimeout(function () {
        subscription.unsubscribe();
    }, 5000);
}

