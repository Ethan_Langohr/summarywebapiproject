﻿function postGame() {
    var req = new XMLHttpRequest();
    var url = 'http://localhost:50966/api/onlinegame/'
    req.open('POST', url)
    req.setRequestHeader("Content-Type", "application/json")

    req.onload = function () {

    }
    req.onerror = function () {
        alert('error')
    }
    var winner;
    if ($('#gridRadios1').is(":checked")) {
        winner = document.getElementById("player1").value;
    }
    else {
        winner = document.getElementById("player2").value;
    }

    var item = {
        GameName: document.getElementById("gameName").value,
        Player1: document.getElementById("player1").value,
        Player2: document.getElementById("player2").value,
        Winner: winner
    }
    console.log(item);

    if (document.getElementById("gameName").value == '' || document.getElementById("player1").value == '' || document.getElementById("player2").value == '') {
        alert("Enter valid values in every field")
    }
    else {
        req.send(JSON.stringify(item));
    }
    
}

function postJQuery() {
    var url = 'http://localhost:50966/api/onlinegame/'

    var winner;
    if ($('#gridRadios1').is(":checked")) {
        winner = $("#player1").val();
    }
    else {
        winner = $("#player2").val();
    }

    var item = {
        GameName: $("#gameName").val(),
        Player1: $("#player1").val(),
        Player2: $("#player2").val(),
        Winner: winner
    }
    console.log(item)

    // JSON.stringify(item)

    var ajaxPostConfiguration = {
        type: "POST", // what is the method? post, get, put , delete
        url: url,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(item) // request http body
    }

    if ($("#gameName").val() == '' || $("#player1").val() == '' || $("#player2").val() == '') {
        alert("Enter valid values in every field")
    }
    else {
        $.ajax(ajaxPostConfiguration).then(
            //success
            function (data) {
                console.log(data)
            }
        ).fail(
            //on error
            function (err) {
                console.error(err)
            }
        )
    }
}
