﻿class WebAPIPromiseClient {
    constructor(realServer) {
        this.realServer = realServer
    }

    fireGetLargeResults() {
        if (this.realServer) {
            var promise = $.ajax(
                {
                    url: 'http://jsonplaceholder.typicode.com/todos'
                })
            return promise
        }

        let testMockServerPromise = $.ajax(
            {
                url: 'http://localhost:3000/api/largeResult'
            })
        return testMockServerPromise
        
    }

    fireGetEmptyResults() {
        if (this.realServer) {
            var promise = $.ajax(
                {
                    url: 'http://jsonplaceholder.typicode.com/todos'
                })
            return promise
        }

        let testMockServerPromise = $.ajax(
            {
                url: 'http://localhost:3000/api/empty'
            })
        return testMockServerPromise

    }
    //to generate an error i request an url, that is not available on the node.js server
    fireGetError() {
        if (this.realServer) {
            var promise = $.ajax(
                {
                    url: 'http://jsonplaceholder.typicode.com/todos'
                })
            return promise
        }

        let testMockServerPromise = $.ajax(
            {
                url: 'http://localhost:3000/api/error'
            })
        return testMockServerPromise

    }
}

function MyObjects(userId, id, title, completed) {
    this.userId = userId
    this.id = id
    this.title = title
    this.completed = completed
    this.stringifyObject = function () {
        return "userid: " + this.userId +
            " id: " + this.id + " title: " + this.title
            + " completed: " + this.completed
    }
}

function WindowAPi() {

    this.testObjects = {}
    

    this.getTodosJQuery = function () {

        var myWebApiPromiseClient = new WebAPIPromiseClient(false)

        var promiseResult = myWebApiPromiseClient.fireGet();

        promiseResult.then(
            (data) => 
            {
                console.log('iam in data')
                this.testObjects = data
                console.log(data)
            },
            (err) => 
            {
                console.log('iam in error')   
                console.log(err)
            }
        )

        myApi.printTestObjects();
    }

    this.printTestObjects = function () {
        console.log(this.testObjects)
        console.log("in print")
        $('#results').empty()
        for (var i = 0; i < this.testObjects.length; i++) {
            var test = new MyObjects(this.testObjects[i].userId,
                this.testObjects[i].id, this.testObjects[i].title,
                this.testObjects[i].completed)
            $('#results').append(test.stringifyObject())
            $('#results').append("<br>")
        }
    }
}

var myApi = new WindowAPi();