﻿function getGameForUpdate() {

    var request = new XMLHttpRequest();
    var url = 'http://localhost:50966/api/onlinegame/' + document.getElementById("ID").value.toString();

    request.open('GET', url)
    request.onload = function () {
        var list = JSON.parse(request.response)
        if (typeof list.ID != 'undefined') {
            document.getElementById("gameName").value = list.GameName
            document.getElementById("player1").value = list.Player1
            document.getElementById("player2").value = list.Player2
            document.getElementById("winner").value = list.Winner
        }
        else if (request.responseText.includes("not found")) {
            document.getElementById("emptyId").innerHTML = "Game with Id " + document.getElementById("ID").value.toString() + " not found"
        }
        else {
            document.getElementById("emptyId").innerHTML = "Please enter valid Id"
        }
    }
    request.onerror = function () {
        alert("Failed to load items from gametable")
    }

    request.send()
}


function putGame() {
    var req = new XMLHttpRequest();
    var url = 'http://localhost:50966/api/onlinegame/' + document.getElementById("ID").value.toString()
    req.open('PUT', url)
    req.setRequestHeader("Content-Type", "application/json")

    req.onload = function () {
    }
    req.onerror = function () {
        alert('error')
    }

    if (document.getElementById("gameName").value == '' || document.getElementById("player1").value == '' || document.getElementById("player2").value == '' || document.getElementById("winner").value == '') {
        alert("Enter valid values in every field")
    }
    else {
        console.log('{"GameName":"' + document.getElementById("gameName").value + '"' + ',"Player1":'
            + '"' + document.getElementById("player1").value + '" ,"Player2":"'
            + document.getElementById("player2").value + '"' + ',"Winner":'
            + '"' + document.getElementById("winner").value + '"}')
        req.send('{"GameName":"' + document.getElementById("gameName").value +  '"' + ',"Player1":'
            + '"' + document.getElementById("player1").value + '" ,"Player2":"'
            + document.getElementById("player2").value + '"' + ',"Winner":'
            + '"' + document.getElementById("winner").value + '"}');
    }
    
}

function getGameJQueryById() {
    $.ajax(
        {
            url: 'http://localhost:50966/api/onlinegame/' + $('#ID').val()
        }).then(
            function (data) {
                var list = data
                console.log(data)
                if (typeof list.ID != 'undefined') {
                    document.getElementById("gameName").value = list.GameName
                    document.getElementById("player1").value = list.Player1
                    document.getElementById("player2").value = list.Player2
                    document.getElementById("winner").value = list.Winner
                    document.getElementById("emptyId").innerHTML = ''
                }
                else {
                    document.getElementById("emptyId").innerHTML = "Please enter valid Id"
                }
            }
    ).fail(
        
        function () { document.getElementById("emptyId").innerHTML = "Game with Id " + document.getElementById("ID").value.toString() + " not found"}
        )
}

function putGameJQuery() {

    var url = 'http://localhost:50966/api/onlinegame/' + $('#ID').val()

    var item = {
        GameName: $("#gameName").val(),
        Player1: $("#player1").val(),
        Player2: $("#player2").val(),
        Winner: $("#winner").val()
    }
    console.log(JSON.stringify(item))

    var ajaxPostDataConfig = {
        type: "PUT", 
        url: url,
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(item) 
    }

    $.ajax(ajaxPostDataConfig).then(
        // success
        function (data) {
            console.log(data)
        }
    ).fail(
        // on error
        function (err) {
            console.error(err)
        }
    )
}
