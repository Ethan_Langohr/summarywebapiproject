﻿function deleteGameById() {
    var req = new XMLHttpRequest();
    var url = 'http://localhost:50966/api/onlinegame/' + document.getElementById("ID").value.toString();
    req.open('DELETE', url)
    req.onload = function () {
        
    }
    req.onerror = function () {
        alert('error: Could not delete selected game')
    }
    req.send()
}



function deleteGameJQuery() {

    var url = 'http://localhost:50966/api/onlinegame/' + $('#ID').val()

    var ajaxPostDataConfig = {
        type: "DELETE",
        url: url
    }

    $.ajax(ajaxPostDataConfig).then(
        // success
        function (data) {
            console.log(data)
        }
    ).fail(
        // on error
        function (err) {
            console.error(err)
        }
    )
}


