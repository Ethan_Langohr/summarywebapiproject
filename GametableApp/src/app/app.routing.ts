import { Routes, RouterModule } from '@angular/router';

import { GameTableSearchComponent } from './game-table-search/game-table-search.component';
import { GameTableViewComponent } from './game-table-view/game-table-view.component';

const APP_ROUTES: Routes = [
{
    path: '', component: GameTableViewComponent
},
{path: 'search', component: GameTableSearchComponent}
];
export const routing = RouterModule.forRoot(APP_ROUTES);
