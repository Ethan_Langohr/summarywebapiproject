import { Component, OnInit, SimpleChanges } from '@angular/core';
import { Http } from '@angular/http';
import { SearchedDataService } from '../searched-data.service';

@Component({
  selector: 'app-game-table-view',
  templateUrl: './game-table-view.component.html',
  styleUrls: ['./game-table-view.component.css']
})
export class GameTableViewComponent implements OnInit {
  displayTableResults = true;
  myResponse;
  isError = false;
  Id: number;
  GameName: string;
  Player1: string;
  Player2: string;
  Winner: string;
  hasPostError = false;
  hasPutError = false;
  hasDeleteError = false;

  foundObject;
  constructor(private _http: Http,  private searchedDataService: SearchedDataService) {
this.getTableValues();
  }



  ngOnInit() {}

  getAjax() {
    this._http
      .get('http://localhost:50966/api/onlinegame/')
      .subscribe(response => {
        this.myResponse = response.json();
        if (this.myResponse) {
          this.isError = true;
        }
      });
  }

  getJSONObjectById(id) {
    for (let _i = 0; _i < this.myResponse.length; _i++) {
      if (this.myResponse[_i].ID === id) {
        return this.myResponse[_i];
      }
    }
  }



  post() {
    this.searchedDataService.postRequest(this.GameName, this.Player1, this.Player2, this.Winner);
    this.Id = null;
    this.GameName = '';
    this.Player1 = '';
    this.Player2 = '';
    this.Winner = '';
    this.searchedDataService.getRequest();
    setTimeout(() => {
        this.myResponse = this.searchedDataService.myResponse;
        this.hasPostError = this.searchedDataService.hasPostError;
        this.hasPutError = this.searchedDataService.hasPutError;
        this.hasDeleteError = this.searchedDataService.hasDeleteError;
    }, 100);
  }

  put() {
    this.searchedDataService.putRequest(this.Id, this.GameName, this.Player1, this.Player2, this.Winner);
    this.Id = null;
    this.GameName = '';
    this.Player1 = '';
    this.Player2 = '';
    this.Winner = '';
    this.searchedDataService.getRequest();
    setTimeout(() => {
        this.myResponse = this.searchedDataService.myResponse;
        this.hasPostError = this.searchedDataService.hasPostError;
        this.hasPutError = this.searchedDataService.hasPutError;
        this.hasDeleteError = this.searchedDataService.hasDeleteError;
    }, 500);

  }

  delete() {
    this.searchedDataService.deleteRequest(this.Id);
    this.Id = null;
    this.GameName = '';
    this.Player1 = '';
    this.Player2 = '';
    this.Winner = '';
    this.searchedDataService.getRequest();
    setTimeout(() => {
        this.myResponse = this.searchedDataService.myResponse;
        this.hasPostError = this.searchedDataService.hasPostError;
        this.hasPutError = this.searchedDataService.hasPutError;
        this.hasDeleteError = this.searchedDataService.hasDeleteError;
    }, 100);
  }
  getTableValues() {
    this.searchedDataService.getRequest();
    setTimeout(() => {
        this.myResponse = this.searchedDataService.myResponse;
    }, 100);
  }

  onClick(event) {
    const idAttr: number = event.target.id;
    const foundObject = this.getJSONObjectById(Number(idAttr));
    this.Id = Number(foundObject.ID);
    this.GameName = foundObject.GameName;
    this.Player1 = foundObject.Player1;
    this.Player2 = foundObject.Player2;
    this.Winner = foundObject.Winner;
  }
}
