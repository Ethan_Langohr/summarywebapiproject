import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameTableViewComponent } from './game-table-view.component';

describe('GameTableViewComponent', () => {
  let component: GameTableViewComponent;
  let fixture: ComponentFixture<GameTableViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameTableViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameTableViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
