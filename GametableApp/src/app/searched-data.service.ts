// tslint:disable-next-line:quotemark
import { Injectable, OnInit } from "@angular/core";
import { Http } from '@angular/http';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
export class SearchedDataService implements OnInit {
  myResponse;
  afterResponse = false;
  hasDeleteError = false;
  hasPutError = false;
  hasPostError = false;
  notFound = false;

  constructor(private _http: Http) {
    this.getRequest();
  }

 // private serviceObject = new BehaviorSubject(this.myResponse);
 // myCurrentResponse = this.serviceObject.asObservable();
  urlToSearch: string;
  ngOnInit() {
  }

  getRequest() {
    this._http
      .get('http://localhost:50966/api/onlinegame/')
      .subscribe(response => {
        this.myResponse = response.json();
      }, );
  }

  SearchFilter(id, GameName, Player1, Player2, Winner) {
    this.notFound = false;
    this.urlToSearch = 'http://localhost:50966/api/onlinegame/search?';

    if (id != null) {
     this.urlToSearch += 'Id=' + id.toString() + '&';
    } else {
      this.urlToSearch += 'Id=-1' + '&';
    }
    if (GameName !== '') {
      this.urlToSearch += 'GameName=' + GameName + '&';
    }
    if (Player1 !== '') {
      this.urlToSearch += 'Player1=' + Player1 + '&';
    }
    if (Player2 !== '') {
      this.urlToSearch += 'Player2=' + Player2 + '&';
    }
    if (Winner !== '') {
      // tslint:disable-next-line:quotemark
      this.urlToSearch += "Winner=" + Winner;
    }
    this._http.get(this.urlToSearch).subscribe(response => {
        this.myResponse = response.json();
        this.afterResponse = true;
      },
      error => {
        this.notFound = true;
      });
  }
  postRequest( GameName, Player1, Player2, Winner) {
    this.hasPostError = false;
    this.hasPutError = false;
    this.hasDeleteError = false;
    this._http.post('http://localhost:50966/api/onlinegame/',
    {
      GameName: GameName,
      Player1: Player1,
      Player2: Player2,
      Winner: Winner
    }).subscribe(response => {
      this.getRequest();
      console.log(response.json());
    },
    error => this.hasPostError = true);
}


putRequest(Id, GameName, Player1, Player2, Winner) {
  this.hasPostError = false;
  this.hasPutError = false;
  this.hasDeleteError = false;
  this._http.put('http://localhost:50966/api/onlinegame/' + Id.toString(),
  {
    GameName: GameName,
    Player1: Player1,
    Player2: Player2,
    Winner: Winner
  }).subscribe(response => {
    this.getRequest();
  },
  error => this.hasPutError = true);
}

deleteRequest(id) {
  this.hasPostError = false;
    this.hasPutError = false;
    this.hasDeleteError = false;
  this._http.delete('http://localhost:50966/api/onlinegame/' + id.toString()).subscribe(
    response => {
      this.getRequest();
      console.log(response.json());
    },
    error => this.hasDeleteError = true
  );
}
}
