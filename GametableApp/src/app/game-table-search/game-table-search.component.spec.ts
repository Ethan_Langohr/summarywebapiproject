import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameTableSearchComponent } from './game-table-search.component';

describe('GameTableSearchComponent', () => {
  let component: GameTableSearchComponent;
  let fixture: ComponentFixture<GameTableSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameTableSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameTableSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
