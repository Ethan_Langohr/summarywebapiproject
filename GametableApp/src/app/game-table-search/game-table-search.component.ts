import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Http } from '@angular/http';
import { SearchedDataService } from '../searched-data.service';

@Component({
  selector: 'app-game-table-search',
  templateUrl: './game-table-search.component.html',
  styleUrls: ['./game-table-search.component.css']
})
export class GameTableSearchComponent implements OnInit {
  isInitalizing = true;
  myResponse;
  displayTableResults = true;
  hasSearched = false;
  Id;
  Gamename = '';
  Player1 = '';
  Player2 = '';
  Winner = '';
  inSearch = true;
  notFound = false;


  foundObject;
  constructor(private _http: Http, private searchedDataService: SearchedDataService) {
    searchedDataService.getRequest();
setTimeout(() => {
    this.myResponse = this.searchedDataService.myResponse;
}, 100);

    }

  ngOnInit() {
  }



  getJSONObjectById(id) {
    for (let _i = 0; _i < this.myResponse.length; _i++) {
      if (this.myResponse[_i].ID === id) {
        return this.myResponse[_i];
      }
    }
  }

  search() {
    this.notFound = false;
    this.searchedDataService.SearchFilter(this.Id, this.Gamename, this.Player1, this.Player2, this.Winner);
    setTimeout(() => {
      if (this.searchedDataService.afterResponse === true) {
        this.myResponse = this.searchedDataService.myResponse;
        this.displayTableResults = true;
      }
      if (this.searchedDataService.notFound === true) {
        this.displayTableResults = false;
        this.notFound = true;
      }
    }, 50);
  }


  onClick(event) {
    const idAttr: number = event.target.id;
    const foundObject = this.getJSONObjectById(Number(idAttr));
    this.Id = Number(foundObject.ID);
    this.Gamename = foundObject.GameName;
    this.Player1 = foundObject.Player1;
    this.Player2 = foundObject.Player2;
    this.Winner = foundObject.Winner;
    this.isInitalizing = false;
  }

}
