import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import {FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { GameTableViewComponent } from './game-table-view/game-table-view.component';
import { GameTableSearchComponent } from './game-table-search/game-table-search.component';
import { routing } from './app.routing';
import { SearchedDataService } from './searched-data.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    GameTableViewComponent,
    GameTableSearchComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [SearchedDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
